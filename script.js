let navbar = document.querySelector('.header .navbar');
let menuBtn = document.querySelector('#menu-btn');

menuBtn.onclick = () =>{
   menuBtn.classList.toggle('fa-times');
   navbar.classList.toggle('active');
};

window.onscroll = () =>{
   menuBtn.classList.remove('fa-times');
   navbar.classList.remove('active');
};

var swiper = new Swiper(".home-slider", {
   grabCursor:true,
   loop:true,
   centeredSlides:true,
   navigation: {
     nextEl: ".swiper-button-next",
     prevEl: ".swiper-button-prev",
   },
});

var swiper = new Swiper(".food-slider", {
   grabCursor:true,
   loop:true,
   centeredSlides:true,
   spaceBetween: 20,
   pagination: {
      el: ".swiper-pagination",
      clickable: true,
   },
   breakpoints: {
      0: {
        slidesPerView: 1,
      },
      700: {
        slidesPerView: 2,
      },
      1000: {
        slidesPerView: 3,
      },
   },
});

let previewContainer = document.querySelector('.food-preview-container');
let previewBox = previewContainer.querySelectorAll('.food-preview');

function validasi1() {
        var nama = document.getElementById("nama").value;
        var menu = document.getElementById("menu").value;
        var catatan = document.getElementById("catatan").value;
        var alamat = document.getElementById("alamat").value;
        var nomor = document.getElementById("nomor").value;
        var jumlah = document.getElementById("jumlah").value;
        var waktu = document.getElementById("waktu").value;
        
        if (nama != "" && menu!= "" && catatan!= "" && alamat!="" && nomor!="" && jumlah!=""&& waktu!="") {
            return true;
        }else{
            alert('Anda harus mengisi data dengan lengkap !');
        }
    }
